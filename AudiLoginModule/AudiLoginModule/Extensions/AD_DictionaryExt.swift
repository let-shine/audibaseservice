//
//  AD_DictionaryExt.swift
//  AudiLoginModule
//
//  Created by KingYoung on 2020/06/9.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit


extension Dictionary {
    func toString() -> String? {
        do {
            let data = try JSONSerialization.data(withJSONObject: self, options: [])
            return String(data: data,encoding:String.Encoding.utf8)
        } catch  {
            ADPrintError(error.localizedDescription)
            return nil
        }
        
    }
}
