//
//  AD_DataExt.swift
//  AudiLoginModule
//
//  Created by KingYoung on 2020/06/9.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit
extension Data {
    func toDictionary() -> [String : Any]? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: .mutableContainers) as? [String : Any]
        } catch {
            ADPrintError(error.localizedDescription)
            return nil
        }
    }
}
