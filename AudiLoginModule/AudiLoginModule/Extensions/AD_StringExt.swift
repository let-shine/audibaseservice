//
//  AD_StringExt.swift
//  AudiLoginModule
//
//  Created by KingYoung on 2020/06/9.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit


extension String {
    func toDictionary() -> [String : Any]? {
        do {
            let data = self.data(using: .utf8)
            return try JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String : Any]
        } catch {
            ADPrintError(error.localizedDescription)
            return nil
        }
    }
}
