//
//  AD_Common.swift
//  AudiLoginModule
//
//  Created by KingYoung on 2020/06/8.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit

struct AD_Constant {
    //UUID
    static let UUID: String = AD_Common.uuid()
}


struct AD_Common {
    // TODO: uuid放 keychain
    static func uuid() -> String {
        let preUUID = UUID().uuidString
        let items = preUUID.components(separatedBy: "-")
        let uuid = items.joined()
        return uuid
    }
}

enum ADPrintType: String {
    case `default` = "🚙"
    case warning = "🚕"
    case error = "🚗"
}


func ADPrint(_ item: Any, file: String = #file, method: String = #function, line: Int = #line, type: ADPrintType = .default) {
    #if DEBUG
    //时间
    let now = Date()
    let dformatter = DateFormatter()
    dformatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
 
    //文件名
    let lastName = ((file as NSString).pathComponents.last!)
    
    //emoji
    var lineBegin = ""
    for _ in 0..<8 {
        lineBegin = lineBegin + type.rawValue
    }
    
    var lineEnd = ""
    for _ in 0..<8 {
        lineEnd = lineEnd + type.rawValue
    }
    
    print("\(dformatter.string(from: now)) [\(lastName)][第\(line)行]\n\(lineBegin)\n\(item)\n\(lineEnd)")
    #else

    #endif
}

func ADPrintError(_ item: Any, file: String = #file, method: String = #function, line: Int = #line, type: ADPrintType = .error) {
    #if DEBUG
    ADPrint(item, file: file, method: method, line: line, type: .error)
    #else
    
    #endif
}

func ADPrintWarning(_ item: Any, file: String = #file, method: String = #function, line: Int = #line, type: ADPrintType = .warning) {
    #if DEBUG
    ADPrint(item, file: file, method: method, line: line, type: .warning)
    #else
    
    #endif
}




