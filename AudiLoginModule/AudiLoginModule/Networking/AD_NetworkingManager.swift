//
//  AD_NetworkingManager.swift
//  AudiLoginModule
//
//  Created by KingYoung on 2020/06/8.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit
import Alamofire
import HandyJSON


struct AD_NetError: Error {
    enum ErrorType {
        case system // 后台返回错误
        case network //网络连接错误
    }
    
    var type: ErrorType
    
    var code: String
    
    var msg: String
    
    var data: [String : Any]?
}




struct AD_RequestConfig {
    enum ContentType: String {
        case `default`, json = "application/json; charset=utf-8"
        case formdata = "application/x-www-form-urlencoded; charset=utf-8"
    }
    
    enum TokenType: String {
        // TODO: token
        case `default` = "xxxx"
      
    }
    
    
    var contentType: ContentType = .default
    var token: TokenType = .default
}


class AD_NetworkingManager: NSObject {
    static let shared = AD_NetworkingManager()
    
//   AD_HttpConfig.headers(needAccessToken: false, path: "", header: .json)
//
//    // MARK: Private
    private override init() {}
    
    
    private func baseRequest<T: HandyJSON, P: Encodable> (withConfig config: AD_RequestConfig,
                                              url: String,
                                              method: HTTPMethod,
                                              parameters: P?,
                                              successHandler: @escaping (Any?)->Void,
                                              failureHander: @escaping (AD_NetError)->Void ,
                                              respMode: T.Type?) {
        
        // 请求, 因为这边是单例 self不会释放，所以用unowned
        AF.request(url, method: method, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: headers(withConfig: config)).responseJSON { [unowned self] (response) in
    
                    
            switch response.result {
            case .success:
                if let data = response.data, let dict = data.toDictionary()  {
                    if let error = self.validateResp(dict) {
                        failureHander(error)
                    } else if let modeDict = dict["data"] as? [String : Any] {
                        if let _ = respMode {
                            let model = T.deserialize(from: modeDict.toString())
                            //返回数据模型
                            successHandler(model)
                        } else {
                            //返回字典
                            successHandler(modeDict)
                        }
                    } else {
                        successHandler(nil)
                    }
                } else {
                    // TODO: 解析错误
                    fatalError()


                }
          
            case .failure:
                fatalError()
                // TODO: 网络错误
//                let statusCode = response.response?.statusCode
                
            }
        }

    }
    
    // 设置headers
    private func headers(withConfig config: AD_RequestConfig) -> HTTPHeaders {
        var headersDict: [String : String] = [
            "Timestamp": "\(Int(NSDate().timeIntervalSince1970 * 1000))",
            "Accept": "application/json",
            "Accept-Language": "zh",
            "deviceId": AD_Constant.UUID]
        
        headersDict["Content-Type"] = config.contentType.rawValue
        headersDict["Authorization"] = config.token.rawValue
        
        return HTTPHeaders(headersDict)
    }
    
    // 校验后台响应
    private func validateResp(_ resp: [String : Any]) -> AD_NetError? {
    
        guard let code = resp["code"] as? String, code != "000000" else {
            return nil
        }
        
        // TODO: "未知错误"
        let message = resp["description"] as? String ?? "未知错误"
        let data = resp["data"] as? [String : Any]
        
        return AD_NetError(type: .system, code: code, msg: message, data: data)
    }
    
    
    // MARK: Public
    
    // Get 请求
    func request_Get<T: HandyJSON, P: Encodable> (withConfig config: AD_RequestConfig = AD_RequestConfig(contentType: .default, token: .default),
                                                       url: String,
                                                       parameters: P?,
                                                       successHandler: @escaping (Any?)->Void,
                                                       failureHander: @escaping (AD_NetError?)->Void ,
                                                       respMode: T.Type?) {
        baseRequest(withConfig: config, url: url, method: .get, parameters: parameters, successHandler: successHandler, failureHander: failureHander, respMode: respMode)
        
    }
    
    // Post 请求
    func request_Post<T: HandyJSON, P: Encodable> (withConfig config: AD_RequestConfig = AD_RequestConfig(contentType: .default, token: .default),
                                                       url: String,
                                                       parameters: P?,
                                                       successHandler: @escaping (Any?)->Void,
                                                       failureHander: @escaping (AD_NetError?)->Void ,
                                                       respMode: T.Type?) {
        baseRequest(withConfig: config, url: url, method: .post, parameters: parameters, successHandler: successHandler, failureHander: failureHander, respMode: respMode)
        
    }
    
    
  
    
    
    

}
