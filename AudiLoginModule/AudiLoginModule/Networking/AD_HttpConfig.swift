//
//  AD_HttpConfig.swift
//  AudiLoginModule
//
//  Created by KingYoung on 2020/06/8.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit

public enum AD_HttpBodyType {
    case json
    case formdata
}

public struct AD_HttpConfig {
    
    static func headers(needAccessToken: Bool = true, path: String = "", header: AD_HttpBodyType = .json) -> [String : String] {
       
        
        
        let accessToken = ""
        var token = ""
//        if path.contains("security/api/v1/app/at/actions/refresh") {
//            token = "Bearer \(tokenModel.refreshToken)" //刷新token接口中,请求头传入RT
//        } else if needAccessToken && !accessToken.isEmpty {
//            token = "Bearer \(accessToken)"
//        }
        var contentType = "application/json; charset=utf-8"
        if header == .formdata {
            contentType = "application/x-www-form-urlencoded; charset=utf-8"
        }
//        let traceId = SVW_RequestHeaderUtility.getTraceId(httpHeaderDid)
        return  [
            "Authorization":  token,
                        "Did": "VW_APP_iPhone_bb48be7c85e54304a405d7b5bb6ae179_11.4.1_0.1.9",
            "Content-Type": contentType,
            "TraceId": "00000000-0000-0000-0000-000000000000_sc_VW_APP_iPhone_bb48be7c85e54304a405d7b5bb6ae179_11.4.1_0.1.9_1591602165437",
            "Timestamp": "\(Int(NSDate().timeIntervalSince1970 * 1000))",
            "Accept": "application/json",
            "Accept-Language": "zh",
            "deviceId": "C31EADBAB4534A689C70AF4BF2BA96DD"
        ]
    }
}
